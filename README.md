# Module for creation of ASGs

This module will create: 

- Launch Configuration
- Auto Scaling Group
- ALB
- Target Group
- DNS record as an alias of the ALB

### Prerequisites
- SSL cert issued in the AWS Certificate manager called `*.domain.com`


### Usage

```
module "asg_demo" {
  source                      = "git::ssh://git@gitlab.com/cloudacio/terraform/asg_module.git?ref=v<tag>"
  instance_name               = "ec2-name"
  name_prefix                 = "prefix-name-"
  domain                      = "dev.domain.com"
  instance_type               = "t3.micro"
  environment                 = "production"
  ebs_optimized               = false
  key_name                    = "jpdentone"
  ec2_security_groups         = ["sg-0fe90573fd9e461aa"]
  alb_security_groups         = ["sg-0279288a0e8b0ff5f"]
  vpc_id                      = "vpc-e9826294"
  subnets_ids                 = ["subnet-a38a5ec5", "subnet-a74b9886"]
  associate_public_ip_address = true
  iam_instance_profile        = "profile"
  user_data_base64            = "<some base64 data here>"
}

### Notes

- The default AMI is the one provided by Amazon with Ubuntu 18.04
- The default root_volume_type is gp2 and root_volume_size is 8GB
- The default IAM role is none
- The ALB wont redirect from port 80 to 443 it will has issues at wp install with the css files
