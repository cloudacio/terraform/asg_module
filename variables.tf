variable "key_name" {
  type        = string
  description = "SSH key pair to be provisioned on the instance"
}

variable "environment" {
  description = "Environment"
}

variable "associate_public_ip_address" {
  type        = bool
  description = "Associate a public IP address with the instance"
  default     = false
}

variable "user_data_base64" {
  type        = string
  description = "Can be used instead of `user_data` to pass base64-encoded binary data directly. Use this instead of `user_data` whenever the value is not a valid UTF-8 string. For example, gzip-encoded user data must be base64-encoded and passed via this argument to avoid corruption"
  default     = null
}

variable "user_data" {
  type        = string
  description = "The user data to provide when launching the instance. Do not pass gzip-compressed data via this argument; see `user_data_base64` instead"
  default     = null
}

variable "name_prefix" {
  type        = string
  description = "Creates a unique name beginning with the specified prefix"
  default     = null
}

variable "instance_type" {
  type        = string
  description = "The type of the instance"
  default     = "t2.micro"
}

variable "iam_instance_profile" {
  type        = string
  description = "The profile name for the instance"
  default     = ""
}

variable "ec2_security_groups" {
  description = "List of Security Group IDs allowed to connect to the instance"
  type        = list(string)
  default     = []
}

variable "alb_security_groups" {
  description = "List of Security Group IDs allowed to connect to the alb"
  type        = list(string)
  default     = []
}

variable "instance_name" {
  type        = string
  description = "Instance Type for tags"
}

variable "ami" {
  type        = string
  description = "The AMI to use for the instance. By default it is the AMI provided by Amazon with Ubuntu 18.04"
  default     = ""
}

variable "ebs_optimized" {
  type        = bool
  description = "Launched EC2 instance will be EBS-optimized"
  default     = false
}

variable "enable_monitoring" {
  type        = bool
  description = "Launched EC2 instance will have detailed monitoring enabled"
  default     = false
}

variable "root_volume_type" {
  type        = string
  description = "Type of root volume. Can be standard, gp2 or io1"
  default     = "gp2"
}

variable "root_volume_size" {
  type        = number
  description = "Size of the root volume in gigabytes"
  default     = 8
}

variable "root_block_device_encrypted" {
  type        = bool
  description = "Whether to encrypt the root block device"
  default     = false
}

variable "delete_on_termination" {
  type        = bool
  description = "Whether the volume should be destroyed on instance termination"
  default     = true
}

variable "kms_key_id" {
  type        = string
  default     = null
  description = "KMS key ID used to encrypt EBS volume. When specifying kms_key_id, ebs_volume_encrypted needs to be set to true"
}

variable "subnets_ids" {
  type        = list(string)
  description = "A list of subnet IDs to launch resources in. Subnets automatically determine which availability zones the group will reside"
  default     = []
}

variable "vpc_id" {
  type = string
}

variable "min_size" {
  type        = number
  description = "The minimum size of the Auto Scaling Group"
  default     = 1
}

variable "desired_capacity" {
  type        = number
  description = "The number of Amazon EC2 instances that should be running in the group"
  default     = 1
}

variable "max_size" {
  type        = number
  description = "The maximum size of the Auto Scaling Group"
  default     = 1
}

variable "health_check_type" {
  type        = string
  description = "EC2 or ELB. Controls how health checking is done"
  default     = "EC2"
}

variable "force_delete" {
  type        = bool
  description = "Allows deleting the Auto Scaling Group without waiting for all instances in the pool to terminate"
  default     = true
}

variable "health_check_grace_period" {
  type        = number
  description = "Time after instance comes into service before checking health"
  default     = 30
}

variable "application_health_path" {
  type        = string
  description = "Path for health checks"
  default     = "/"
}

variable "application_port" {
  type        = number
  description = "Application Port"
  default     = 80
}

variable "alb_idle_timeout" {
  type        = number
  description = "The time in seconds that the connection is allowed to be idle"
  default     = 120
}

variable "domain" {
  type        = string
  description = "Application Domain"
}

variable "sub_domain" {
  type        = string
  description = "Application Domain"
  default     = ""
}