data "aws_acm_certificate" "default" {
  domain   = "*.${var.domain}"
  statuses = ["ISSUED"]
}

resource "aws_lb_target_group" "default" {
  name                 = "${var.instance_name}"
  port                 = var.application_port
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  deregistration_delay = "1"

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "true"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 15
    path                = var.application_health_path
    matcher             = "200,302"
  }

  tags = {
    environment = var.environment
  }
}

resource "aws_lb" "default" {
  name                       = "${var.instance_name}"
  load_balancer_type         = "application"
  security_groups            = var.alb_security_groups
  subnets                    = var.subnets_ids
  enable_deletion_protection = false
  idle_timeout               = var.alb_idle_timeout

  tags = {
    environment = var.environment
  }
}

resource "aws_lb_listener" "alb_listener_https" {
  load_balancer_arn = aws_lb.default.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = data.aws_acm_certificate.default.arn

  default_action {
    target_group_arn = aws_lb_target_group.default.arn
    type             = "forward"
  }
}

resource "aws_lb_listener" "alb_listener_http" {
  load_balancer_arn = aws_lb.default.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.default.arn
    type             = "forward"
  }
}
