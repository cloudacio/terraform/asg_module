output "lb_zone_id" {
  description = "LB zone id"
  value       = aws_lb.default.zone_id
}

output "lb_dns_name" {
  description = "LB DNS name"
  value       = aws_lb.default.dns_name
}

output "service_dns_name" {
  description = "Service DNS name"
  value       = aws_route53_record.default.name
}

output "asg_name" {
  description = "ASG name"
  value       = aws_autoscaling_group.default.name
}