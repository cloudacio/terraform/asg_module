data "aws_route53_zone" "default" {
  name = var.domain
}

locals {
  service_public_name = var.sub_domain != "" ? "${var.sub_domain}.${var.domain}" : var.domain
}

resource "aws_route53_record" "default" {
  zone_id = data.aws_route53_zone.default.zone_id
  name    = local.service_public_name
  type    = "A"

  alias {
    name                   = aws_lb.default.dns_name
    zone_id                = aws_lb.default.zone_id
    evaluate_target_health = true
  }
}
